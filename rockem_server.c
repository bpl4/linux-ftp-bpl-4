// Brian Le: bpl4@pdx.edu
// R. Chaney: rchaney@pdx.edu
// This FTP Rockem Socket program was written with the help of Professor Chaney and information from "The Linux Programming Interface" textbook
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/uio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <limits.h>
#include <crypt.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <stdint.h>

#include "rockem_hdr.h"

#define LISTENQ 100

void process_connection(int sockfd, void *buf, int n, char*, char*);
void *thread_get(void *p);
void *thread_put(void *p);
void *thread_dir(void *p);
void *thread_cd(void *p);
void *server_commands(void *p);
void current_connections_inc(void);
void current_connections_dec(void);
unsigned int current_connections_get(void);
void server_help(void);

static short is_verbose = 0;
static unsigned sleep_flag = 0;
static unsigned long tcount = 0;
static unsigned int current_connections = 0;
static pthread_mutex_t connections_mutex = PTHREAD_MUTEX_INITIALIZER;

void sigint_handler(int);
void exit_handler(void);

int
main(int argc, char *argv[]) 
{
    int listenfd;
    int sockfd;
    int n;
    char buf[MAXLINE];
    socklen_t clilen;
    struct sockaddr_in cliaddr;
    struct sockaddr_in servaddr;
    short ip_port = DEFAULT_SERV_PORT;
    int opt;

    char pass[100] = {"\0"};
    char username[100] = {"\0"};
    //char settings[CRYPT_OUTPUT_SIZE] = {"\0"};
    char salt[CRYPT_OUTPUT_SIZE] = {"\0"};
    unsigned int seed = 3;
    char salt_chars[] = {SALT_CHARS};
    char* crypt_ret = NULL;
    char hashed_pass[CRYPT_OUTPUT_SIZE] = {"\0"};

    struct crypt_data crypt_stuff;
    pthread_t cmd_thread;
    pthread_mutex_init(&connections_mutex, NULL);

    while ((opt = getopt(argc, argv, SERVER_OPTIONS)) != -1) {
        switch (opt) {
        case 'p':
            // CONVERT and assign optarg to ip_port
	    sscanf(optarg, "%hd", &ip_port);
            break;
        case 'u':
            // add 1000 to sleep_flag
	    sleep_flag += 1000;
            break;
        case 'v':
            is_verbose++;
            break;
        case 'h':
            fprintf(stderr, "%s ...\n\tOptions: %s\n"
                    , argv[0], SERVER_OPTIONS);
            fprintf(stderr, "\t-p #\t\tport on which the server will listen (default %hd)\n"
                    , DEFAULT_SERV_PORT);
            fprintf(stderr, "\t-u\t\tnumber of thousands of microseconds the server will sleep between "
                    "read/write calls (default %d)\n"
                    , sleep_flag);
            fprintf(stderr, "\t-v\t\tenable verbose output. Can occur more than once to increase output\n");
            fprintf(stderr, "\t-h\t\tshow this rather lame help message\n");
            exit(EXIT_SUCCESS);
            break;
        default:
            fprintf(stderr, "*** Oops, something strange happened <%s> ***\n", argv[0]);
            break;
        }
    }

    // require a username login
    printf("Enter a username to login: ");
    fgets(username, 100, stdin);
    username[strcspn(username, "\n")] = '\0';

    // require the password
    strcpy(pass, getpass("Enter a password for clients to connect: "));
    /*
    if (strlen(pass) == 0) {
	    fprintf(stderr, "Must provide a password for security\n");
	    exit(EXIT_FAILURE);
    }
    */


    // hash the password
    memset(salt, 0, CRYPT_OUTPUT_SIZE);
    for (int i = 0; i < MAX_YES_SALT_LEN; ++i)
    {
	    salt[i] = salt_chars[rand_r(&seed) % (sizeof(salt_chars) - 1)];
    }
    sprintf(crypt_stuff.setting, "$y$j9T$%.*s$", (int) strlen(salt), salt);
    strcpy(crypt_stuff.input, pass);
    //strcpy(crypt_stuff.setting, settings);
    crypt_ret = crypt_rn(crypt_stuff.input, crypt_stuff.setting, &(crypt_stuff), (int) sizeof(crypt_stuff));
    if (crypt_ret == NULL)
    {
	    perror("crypt_rn failed");
	    exit(EXIT_FAILURE);
    }

    if (is_verbose)
	    fprintf(stderr, "Hashed output: %s\n", crypt_stuff.output);
    
    strcpy(hashed_pass, crypt_stuff.output);
    memset(&crypt_stuff, 0, sizeof(crypt_stuff));

    signal(SIGINT, sigint_handler);

    // Create a socket from the AF_INET family, that is a stream socket
    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    
    // Performing a memset() on servaddr is quite important when doing 
    //   socket communication.
    memset(&servaddr, 0, sizeof(servaddr));

    // An IPv4 address
    servaddr.sin_family = AF_INET;
    
    // Host-TO-Network-Long. Listen on any interface/IP of the system.
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // Host-TO-Network-Short, the default port from above.
    servaddr.sin_port = htons(ip_port);

    
    // bind the listenfd
    if (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0)
    {
	    perror("Socket binding failed");
	    exit(EXIT_FAILURE);
    }

    // listen on the listenfd
    listen(listenfd, LISTENQ);
    if (is_verbose)
	    fprintf(stderr, "Listening on %hd\n", ip_port); 

    {
        char hostname[256];
        struct hostent *host_entry = NULL;
        char *IPbuffer = NULL;
	//struct in_addr addr;

        memset(hostname, 0, sizeof(hostname));
        // gethostname()
	if (gethostname(hostname, 256) == -1)
	{
		fprintf(stderr, "gethostname() failed\n");
	}

        // gethostbyname()
	if ((host_entry = gethostbyname(hostname)) == NULL)
	{
		fprintf(stderr, "gethostbyname() failed\n");
	}

        // inet_ntoa()
	/*
	if (inet_aton(host_entry->h_addr_list[0], &addr) == 0)
		fprintf(stderr, "inet_aton() failed\n");


	if ((IPbuffer = inet_ntoa(addr)) == NULL)
	{
		fprintf(stderr, "inet_ntoa() failed\n");
	}
	*/

	if ((IPbuffer = inet_ntoa(*((struct in_addr *)host_entry->h_addr_list[0]))) == NULL)
		fprintf(stderr, "inet_ntoa() failed\n");
        
        fprintf(stdout, "Hostname: %s\n", hostname);
        fprintf(stdout, "IP:       %s\n", IPbuffer);
        fprintf(stdout, "Port:     %d\n", ip_port);
    }

    // create the input handler thread
    pthread_create(&(cmd_thread), NULL, server_commands, NULL); // note to self: figure out what should be passed in the function.
    
    // client length
    clilen = sizeof(cliaddr);

    // Accept connections on the listenfd.
    //sockfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);

    for ( ; ; ) {
        // loop forever accepting connections
	sockfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
	if (sockfd < 0) {
		perror("accept() failed");
	}

        // You REALLY want to memset to all zeroes before you get bytes from
        // the socket.
        memset(buf, 0, sizeof(buf));

        // read a cmd_t structure from the socket.
        // if zro bytes are read, close the scoket
	//cmd_t cmd;
	//memset(&cmd, 0, sizeof(cmd_t));
        if ((n = read(sockfd, buf, sizeof(buf))) == 0) {
            fprintf(stdout, "EOF found on client connection socket, "
                    "closing connection.\n");
            // nothing was read, EOF
            // close the scoket
	    close(sockfd);
        }
        else {
	    //strcpy(buf, cmd.cmd);
            if (is_verbose) {
                fprintf(stdout, "Connection from client: <%s>\n", buf);
            }
            // process the command from the client
            // in the process_connection() is where I divy out the put/get/dir
            // threads
            process_connection(sockfd, buf, n, hashed_pass, username);
        }
    }

    printf("Closing listen socket\n");
    close(listenfd);

    // this could be pthread_exit, I guess...
    return(EXIT_SUCCESS);
}

void
process_connection(int sockfd, void *buf, int n, char* pass, char* username)
{
    // I have to allocate one of these for each thread that is created.
    // The thread is responsible for calling free on it.
    cmd_t *cmd = (cmd_t *) malloc(sizeof(cmd_t));
    int ret;
    pthread_t tid;
    //pthread_attr_t attr;

    memcpy(cmd, buf, sizeof(cmd_t));

    // check the password and username
    if (strcmp(cmd->username, username) != 0) {
	    close(sockfd);
	    if (is_verbose)
		    fprintf(stderr, "Invalid username was entered by client. Closing this connection\n.");
	    return;
    }

    if (strcmp(cmd->pass, pass) != 0) {
	//char msg[] = "Invalid Password\n";
	//write(sockfd, msg, strlen(msg));
	close(sockfd);
	if (is_verbose)
		fprintf(stderr, "Invalid password was entered by client. Closing this connection\n.");
	return;
    }

    cmd->sock = sockfd;
    //cmd->tcount = tcount++;
    if (is_verbose) {
        fprintf(stderr, "Request from client: <%s> <%s>\n"
                , cmd->cmd, cmd->name);
    }

    // lock
    pthread_mutex_lock(&connections_mutex);
    // increment the totaol conections vrariable
    tcount += 1;
    pthread_mutex_unlock(&connections_mutex);

    if (strcmp(cmd->cmd, CMD_GET) == 0) {
	if (is_verbose)
		fprintf(stderr, "Processing the GET command\n");
        // create thread to handle get file
	ret = pthread_create(&(tid), NULL, thread_get, cmd);
        if (ret < 0) {
            fprintf(stderr, "ERROR: %d\n", __LINE__);
        }
    }
    else if (strcmp(cmd->cmd, CMD_PUT) == 0) {
	if (is_verbose)
		fprintf(stderr, "Processing the PUT command\n");
        // create thread to handle put file
	ret = pthread_create(&(tid), NULL, thread_put, cmd);
        if (ret < 0) {
            fprintf(stderr, "ERROR: %d\n", __LINE__);
        }
    }
    else if (strcmp(cmd->cmd, CMD_DIR) == 0) {
	if (is_verbose)
		fprintf(stderr, "Processing DIR command\n");

        // create thread to handle dir
	ret = pthread_create(&(tid), NULL, thread_dir, cmd);

        if (ret < 0) {
            fprintf(stderr, "ERROR: %d\n", __LINE__);
        }
    }
    else if (strcmp(cmd->cmd, CMD_CD) == 0) {
	    if (is_verbose)
		    fprintf(stderr, "Changing Directory going to %s\n", cmd->directory);

	    ret = pthread_create(&(tid), NULL, thread_cd, cmd);

	    if (ret < 0)
		    fprintf(stderr, "ERROR: %d\n", __LINE__);
    }
    else {
        // This should never happen since the checks are made on 
        // the client side.
        fprintf(stderr, "ERROR: unknown command >%s< %d\n", cmd->cmd, __LINE__);
        // close the socket
	close(cmd->sock);
    }
}

void *
server_commands(void *p)
{
    char cmd[80];
    char *ret_val;

    // detach the thread
    pthread_detach(pthread_self());

    server_help();
    for ( ; ; ) {
        fputs(">> ", stdout);
        ret_val = fgets(cmd, sizeof(cmd), stdin);
        if (ret_val == NULL) {
            // end of input, a control-D was pressed.
            break;
        }
        // STOMP on the pesky new line
	cmd[strlen(cmd) - 1] = '\0';

        if (strlen(cmd) == 0) {
            continue;
        }
        else if (strcmp(cmd, SERVER_CMD_EXIT) == 0) {
            // I really should do something better than this.
            break;
        }
        else if (strcmp(cmd, SERVER_CMD_COUNT) == 0) {
            printf("total connections   %lu\n", tcount);
            printf("current connections %u\n", current_connections_get());
            printf("verbose             %d\n", is_verbose);
        }
        else if (strcmp(cmd, SERVER_CMD_VPLUS) == 0) {
            is_verbose++;
            printf("verbose set to %d\n", is_verbose);
        }
        else if (strcmp(cmd, SERVER_CMD_VMINUS) == 0) {
            is_verbose--;
            if (is_verbose < 0) {
                is_verbose = 0;
            }
            printf("verbose set to %d\n", is_verbose);
        }
        else if (strcmp(cmd, SERVER_CMD_HELP) == 0) {
            server_help();
        }
        else {
            printf("command not recognized >>%s<<\n", cmd);
        }
    }

    // This is really harsh. It terminates on all existing threads.
    // This would probably be better with a good exit hander
    exit(EXIT_SUCCESS);
}

void
server_help(void)
{
    printf("available commands are:\n");
    printf("\t%s : show the total connection count "
           "and number current connection\n"
           , SERVER_CMD_COUNT);
    printf("\t%s    : increment the is_verbose flag (current %d)\n"
           , SERVER_CMD_VPLUS, is_verbose);
    printf("\t%s    : decrement the is_verbose flag (current %d)\n"
           , SERVER_CMD_VMINUS, is_verbose);
    printf("\t%s  : exit the server process\n"
           , SERVER_CMD_EXIT);
    printf("\t%s  : show this help\n"
           , SERVER_CMD_HELP);
}

// get from server, so I need to send data to the client.
void *
thread_get(void *p)
{
    cmd_t *cmd = (cmd_t *) p;
    int fd;
    ssize_t bytes_read;
    char buffer[MAXLINE];

    current_connections_inc();

    if (is_verbose) {
        fprintf(stderr, "Sending %s to client\n", cmd->name);
    }
    // ope the file in cmd->name, read-only
    fd = open(cmd->name, O_RDONLY);
    if (fd < 0) {
        // barf
	fprintf(stderr, "open() failed\n");
        // close things up, free() things up and leave
	close(fd);
	close(cmd->sock);
	free(cmd);
        pthread_exit((void *) EXIT_FAILURE);
    }
    // in a while loop, read from the file and write to the socket
    // within the while loop, if sleep_flap > 0, usleep()
    memset(buffer, 0, sizeof(buffer));
    while((bytes_read = read(fd, buffer, MAXLINE)) > 0)
    {
	    write(cmd->sock, buffer, bytes_read);
	    if (sleep_flag > 0)
		    usleep(sleep_flag);
    }

    // close file descriptor
    close(fd);
    // close socket
    close(cmd->sock);
    // free
    free(cmd);

    current_connections_dec();

    pthread_exit((void *) EXIT_SUCCESS);
}

void *
thread_put(void *p)
{
    cmd_t *cmd = (cmd_t *) p;
    int fd;
    ssize_t bytes_read = 0;
    ssize_t bytes_written = 0;
    ssize_t bytes_toclient = 0;
    char buffer[MAXLINE];
    //int loop = 1;

    current_connections_inc();

    if (is_verbose) {
        fprintf(stderr, "VERBOSE: Receiving %s from client\n"
                , cmd->name);
    }
    // open the file in cmd->name as write-only
    // truncate it is it aready exists
    fd = open(cmd->name, O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR);
    chmod(cmd->name, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    if (fd < 0) {
        // barf
	fprintf(stderr, "open() failed\n");
        // close things up, free() things up and leave
	close(fd);
	close(cmd->sock);
	free(cmd);
        pthread_exit((void *) EXIT_FAILURE);
    }

    // in a while loop, read from the scoket and write to the file
    // within the while loop, if sleep_flap > 0, usleep()
    memset(buffer, 0, sizeof(buffer));
    while((bytes_read = recv(cmd->sock, buffer, MAXLINE, 0)) > 0)
    {
	    if (is_verbose)
		    printf("%ld bytes read from socket %d\n", bytes_read, cmd->sock);

	    bytes_written += write(fd, buffer, bytes_read);

	    if (sleep_flag > 0)
		    usleep(sleep_flag);

	    if (bytes_written == cmd->sb.st_size)
	    {
		    cmd->file_written = bytes_written;
		    if ((bytes_toclient = write(cmd->sock, cmd, sizeof(cmd_t))) > 0)
			    fprintf(stderr, "Sent the client info back: %ld\n", cmd->file_written);
		    else if (bytes_toclient < 0)
			    perror("Could not send client info back");
		    break;
	    }

    }

    if (bytes_read < 0)
	    perror("Failed to read from socket");

    fprintf(stderr, "Done reading from sock\n");

    if (is_verbose)
    {
	    printf("%ld bytes written.\n", bytes_written);
	    printf("%ld bytes read total\n", bytes_read);
	    fprintf(stderr, "original: %ld  written: %ld\n", cmd->sb.st_size, bytes_written);
    }

    if (bytes_written != cmd->sb.st_size) // do it again if the file size are not the same
    {
	    if (is_verbose)
	    {
		    fprintf(stderr, "Packets were lost, redownloading from socket %d\n", cmd->sock);
	    }
    }

    /* send info back to the client.
    if ((write(cmd->sock, &bytes_written, sizeof(ssize_t))) < 0)
    {
	    perror("write() to send info back to client failed");
    }
    
    else
	    fprintf(stderr, "Sent the client info back\n");
	    */


    // close file descriptor
    close(fd);
    // close socket
    if (is_verbose)
	    fprintf(stderr, "Closing cmd socket\n");

    close(cmd->sock);
    // free
    free(cmd);

    current_connections_dec();

    pthread_exit((void *) EXIT_SUCCESS);
}

void *
thread_dir(void *p)
{
    cmd_t *cmd = (cmd_t *) p;
    FILE *fp;
    char buffer[MAXLINE];

    if (is_verbose)
    	fprintf(stderr, "Executing thread_dir()\n");

    current_connections_inc();

    fp = popen(CMD_DIR_POPEN, "r");
    if (fp == NULL) {
        // barf
        // close, free, skedaddle
	pclose(fp);
	close(cmd->sock);
	free(cmd);

        pthread_exit((void *) EXIT_FAILURE);
    }
    memset(buffer, 0, sizeof(buffer));

    // in a while loop, read from fp, write to the socket
    while((fgets(buffer, MAXLINE, fp)) != NULL)
    {
	write(cmd->sock, buffer, strlen(buffer));
    }

    // pclose
    pclose(fp);
    // close the socket
    close(cmd->sock);
    // free
    free(cmd);

    current_connections_dec();

    if (is_verbose)
	    fprintf(stderr, "Finished processing DIR command. Thread exiting.\n");

    pthread_exit((void *) EXIT_SUCCESS);
}

void*
thread_cd(void *p) {
	cmd_t *cmd = (cmd_t *) p;
	char path[PATH_MAX] = {"\0"};

	current_connections_inc();

	if (strlen(cmd->directory) == 0) // go to home directory
	{
		if (is_verbose) {
			fprintf(stderr, "going to home directory\n");
		}

		if (chdir(getenv("HOME")) < 0)
		{
			perror("Failed to change directory");
		}
	}

	else // go to some directory.
	{
		if (is_verbose) {
			fprintf(stderr, "going to directory: %s\n", cmd->directory);
		}

		getcwd(path, PATH_MAX); // gets the current working directory as a string.
		//strcat(path, cmd->directory);

		if (chdir(cmd->directory) < 0) {
			perror("Failed to change directory");
		}
	}

	close(cmd->sock);

	free(cmd);

	current_connections_dec();

	pthread_exit((void *) EXIT_SUCCESS);
}
// I should REALLY put these fucntions and their related variables
// in a seperate source file.
void
current_connections_inc(void)
{
	if (is_verbose)
		fprintf(stderr, "Incrementing current connections\n");
    // lock
	pthread_mutex_lock(&connections_mutex);
    // increment
	current_connections += 1;
    // unlock
	pthread_mutex_unlock(&connections_mutex);
}

void
current_connections_dec(void)
{
	if (is_verbose)
		fprintf(stderr, "Decrementing current connections\n");
    // lock
	pthread_mutex_lock(&connections_mutex);
    // decrement
	current_connections -= 1;
    // unlock
	pthread_mutex_unlock(&connections_mutex);
}

unsigned int
current_connections_get(void)
{
    return current_connections;
}

void sigint_handler(int signo)
{
	if (is_verbose)
		fprintf(stderr, "Control-C was entered.\n");
	exit(EXIT_SUCCESS);
}

void exit_handler(void)
{
	if (is_verbose)
		fprintf(stderr, "exit command or Control-D was entered.\n");
}
