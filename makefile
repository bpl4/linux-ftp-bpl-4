CC = gcc -g -Wall
CFLAGS = -pthread \
-Wall -Wextra -Wshadow -Wunreachable-code \
-Wredundant-decls -Wmissing-declarations \
-Wold-style-definition -Wmissing-prototypes \
-Wdeclaration-after-statement -Wno-return-local-addr \
-Wunsafe-loop-optimizations -Wuninitialized -Werror \
-Wno-unused-parameter \


CLIENT = rockem_client
SERVER = rockem_server
HEADER = rockem_hdr

all: $(CLIENT) $(SERVER)

$(CLIENT): $(CLIENT).o
	$(CC) $(CFLAGS) -o $@ $^ -lcrypt

$(CLIENT).o: $(CLIENT).c $(HEADER).h
	$(CC) $(CFLAGS) -c $<

$(SERVER): $(SERVER).o
	$(CC) $(CFLAGS) -o $@ $^ -lcrypt

$(SERVER).o: $(SERVER).c $(HEADER).h
	$(CC) $(CFLAGS) -c $<

clean cls:
	rm -f $(CLIENT) $(SERVER) *.o \#* *~

git:
	git add $(CLIENT).c $(SERVER).c $(HEADER).h [mM]akefile
	git status

tar:
	tar cvfa ${LOGNAME}-rockem_sockets.tar.gz $(CLIENT).c $(SERVER).c $(HEADER).h [mM]akefile
