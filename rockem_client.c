// Brian Le: bpl4@pdx.edu
// R. Chaney: rchaney@pdx.edu
// This FTP Rockem Socket program was written with the help of Professor Chaney and information from "The Linux Programming Interface" textbook
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/uio.h>
#include <unistd.h>
#include <pthread.h>
#include <limits.h>
#include <crypt.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <stdint.h>
#include <signal.h>

#include "rockem_hdr.h"

static unsigned short is_verbose = 0;
static unsigned sleep_flag = 0;

//static char ip_addr[50] = "131.252.208.23"; this is the default ip address of PSU.
static char ip_addr[50] = {"\0"};
static short ip_port = DEFAULT_SERV_PORT;
static char hash_pass[CRYPT_OUTPUT_SIZE] = {"\0"};
static char username[100] = {"\0"};

int get_socket(char *, int);
void get_file(char *);
void put_file(char *);
void *thread_get(void *);
void *thread_put(void *);
void list_dir(void);
void change_dir(char *);

int
main(int argc, char *argv[])
{
    cmd_t cmd;
    int opt;
    pthread_t* threads = NULL;

    //char settings[CRYPT_OUTPUT_SIZE] = {"\0"};
    char salt[CRYPT_OUTPUT_SIZE] = {"\0"};
    unsigned int seed = 3;
    char salt_chars[] = {SALT_CHARS};
    char* crypt_ret = NULL;
    char pass[100] = {"\0"};

    struct crypt_data crypt_stuff;

    memset(&cmd, 0, sizeof(cmd_t));
    while ((opt = getopt(argc, argv, CLIENT_OPTIONS)) != -1) {
        switch (opt) {
        case 'i':
            // copy optarg into the ip_addr
	    strcpy(ip_addr, optarg);
            break;
        case 'p':
            // CONVERT and assign optarg to ip_port
	    sscanf(optarg, "%hd", &ip_port);
            break;
        case 'c':
            // copy optarg into data member cmd.cmd
	    strcpy(cmd.cmd, optarg);
            break;
        case 'v':
            is_verbose++;
            break;
        case 'u':
            // add 1000 to sleep_flag
	    sleep_flag += 1000;
            break;
	case 'l':
	    strcpy(pass, optarg);
	    break;
	case 'n':
	    strcpy(username, optarg);
	    break;
        case 'h':
            fprintf(stderr, "%s ...\n\tOptions: %s\n"
                    , argv[0], CLIENT_OPTIONS);
            fprintf(stderr, "\t-i str\t\tIPv4 address of the server (default %s)\n"
                    , ip_addr);
            fprintf(stderr, "\t-p #\t\tport on which the server will listen (default %hd)\n"
                    , DEFAULT_SERV_PORT);
	    fprintf(stderr, "\t-l str\t\tThe password that will validate if this client can connect to the server\n");
            fprintf(stderr, "\t-c str\t\tcommand to run (one of %s, %s, %s, %s)\n"
                    , CMD_GET, CMD_PUT, CMD_DIR, CMD_CD);
            fprintf(stderr, "\t-u\t\tnumber of thousands of microseconds the client will sleep between read/write calls (default %d)\n"
                    , 0);
            fprintf(stderr, "\t-v\t\tenable verbose output. Can occur more than once to increase output\n");
            fprintf(stderr, "\t-h\t\tshow this rather lame help message\n");
            exit(EXIT_SUCCESS);
            break;
        default:
            fprintf(stderr, "*** Oops, something strange happened <%s> ***\n", argv[0]);
            break;
        }
    }

    if (strlen(ip_addr) == 0) {
	    fprintf(stderr, "Must provide a public IPv4 address\n");
	    exit(EXIT_FAILURE);
    }

    if (strlen(pass) == 0) {
	    fprintf(stderr, "Must provide a password\n");
	    exit(EXIT_FAILURE);
    }

    // hash the password
    memset(salt, 0, CRYPT_OUTPUT_SIZE);
    for (int i = 0; i < MAX_YES_SALT_LEN; ++i)
    {
	    salt[i] = salt_chars[rand_r(&seed) % (sizeof(salt_chars) - 1)];
    }
    //sprintf(settings, "$y$j9T$%.*s$", (int) strlen(salt), salt);
    sprintf(crypt_stuff.setting, "$y$j9T$%.*s$", (int) strlen(salt), salt);
    strcpy(crypt_stuff.input, pass);
    memset(pass, 0, 100);
    //strcpy(crypt_stuff.setting, settings);
    crypt_ret = crypt_rn(crypt_stuff.input, crypt_stuff.setting, &(crypt_stuff), (int) sizeof(crypt_stuff));
    if (crypt_ret == NULL)
    {
	    perror("crypt_rn failed");
	    exit(EXIT_FAILURE);
    }


    if (is_verbose)
	    fprintf(stderr, "Hashed output: %s\n", crypt_stuff.output);

    strcpy(hash_pass, crypt_stuff.output);
    memset(&crypt_stuff, 0, sizeof(crypt_stuff));

    threads = malloc(sizeof(pthread_t) * (argc - optind));

    if (is_verbose)
    	fprintf(stderr, "There are %d files given.\n", ((argc - optind)));

    if (is_verbose) {
        fprintf(stderr, "Command to server: <%s> %d\n"
                , cmd.cmd, __LINE__);
    }
    if (strcmp(cmd.cmd, CMD_GET) == 0) {
        // process the files left on the command line, creating a threas for
        // each file to connect to the server
        for (int i = 0; i < argc - optind; i++) {
            // create threads
            // pass the file name as the ONE parameter to the thread function
	    pthread_create(&(threads[i]), NULL, thread_get, argv[optind + i]);
        }
    }
    else if (strcmp(cmd.cmd, CMD_PUT) == 0) {
        // process the files left on the command line, creating a threas for
        // each file to connect to the server
        for (int j = 0; j < argc - optind; j++) {
            // create threads
            // pass the file name as the ONE parameter to the thread function
	    pthread_create(&(threads[j]), NULL, thread_put, argv[optind + j]);
        }
    }
    else if (strcmp(cmd.cmd, CMD_DIR) == 0) {
	    list_dir();
    }
    else if (strcmp(cmd.cmd, CMD_CD) == 0) {
	    if (argc > optind)
		    change_dir(argv[optind]);
	    else
		    change_dir("");
    }
    else {
        fprintf(stderr, "ERROR: unknown command >%s< %d\n", cmd.cmd, __LINE__);
	free(threads);
        exit(EXIT_FAILURE);
    }

    free(threads);

    pthread_exit(NULL);
}

int
get_socket(char * addr, int port)
{
    // configure and create a new socket for the connection to the server
    int sockfd;
    struct sockaddr_in servaddr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&servaddr, 0, sizeof(servaddr));

    // more stuff in here
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    inet_pton(AF_INET, addr, &servaddr.sin_addr.s_addr);

    if (connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) != 0)
    {
	    perror("could not connect");
	    exit(EXIT_FAILURE);
    }

    return sockfd;
}

// get one file
void
get_file(char *file_name)
{
    cmd_t cmd;
    int sockfd;
    int fd;
    ssize_t bytes_read;
    char buffer[MAXLINE];

    strcpy(cmd.cmd, CMD_GET);
    strcpy(cmd.pass, hash_pass);
    strcpy(cmd.username, username);
    if (is_verbose) {
        fprintf(stderr, "next file: <%s> %d\n", file_name, __LINE__);
    }
    strcpy(cmd.name, file_name);
    if (is_verbose) {
        fprintf(stderr, "get from server: %s %s %d\n", cmd.cmd, cmd.name, __LINE__);
    }

    // get the new socket to the server (get_socket(...)
    sockfd = get_socket(ip_addr, ip_port);

    // write the command to the socket
    write(sockfd, &cmd, sizeof(cmd)); 

    // open the file to write
    fd = open(file_name, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR, S_IWUSR);
    chmod(file_name, S_IRUSR | S_IWUSR);

    // loop reading from the socket, writing to the file
    //   until the socket read is zero
    memset(buffer, 0, sizeof(buffer));
    while((bytes_read = read(sockfd, buffer, MAXLINE)) > 0)
    {
	    write(fd, buffer, bytes_read);
    	    if (sleep_flag > 0)
		    usleep(sleep_flag);
    }
    
    if (bytes_read < 0)
    {
	    perror("Read from sockfd failed");
	    fprintf(stderr, "Invalid password or unable to connect to server\n");
    }

    // close the file
    close(fd);
    // close the socket
    close(sockfd);
}

void
put_file(char *file_name)
{
    cmd_t cmd;
    cmd_t cmd_ret;
    int sockfd;
    int fd;
    ssize_t bytes_read = 0;
    ssize_t bytes_written = 0;
    char buffer[MAXLINE];
    //char file_size_return[MAXLINE] = {'\0'};
    ssize_t sent_file_size = 0;
    ssize_t server_read = 0;
    //int loop = 1;
    pthread_t tid;


    strcpy(cmd.cmd, CMD_PUT);
    strcpy(cmd.pass, hash_pass);
    strcpy(cmd.username, username);
    if (is_verbose) {
        fprintf(stderr, "next file: <%s> %d\n", file_name, __LINE__);
    }
    strcpy(cmd.name, file_name);
    if (is_verbose) {
        fprintf(stderr, "put to server: %s %s %d\n", cmd.cmd, cmd.name, __LINE__);
    }
    
    if (lstat(file_name, &(cmd.sb)) == -1) {
		    perror("lstat failed");
    }

    memset(&cmd_ret, 0, sizeof(cmd_t));

    while (cmd.sb.st_size > cmd_ret.file_written)
    {

	    // get the new socket to the server (get_socket(...)
	    sockfd = get_socket(ip_addr, ip_port); 
	    if (sockfd < 0) {
		    perror("Failed to get socket\n");
	    }
	    
	    
	    // write the command to the socket
	    write(sockfd, &cmd, sizeof(cmd_t));

	    // open the file for read
	    fd = open(file_name, O_RDONLY);
	    chmod(cmd.name, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	    // loop reading from the file, writing to the socket
	    //   until file read is zero
	    memset(buffer, 0, sizeof(buffer));
	    bytes_written = 0;
	    
	    while((bytes_read = read(fd, buffer, MAXLINE)) > 0)
	    {
		    bytes_written += send(sockfd, buffer, bytes_read, MSG_NOSIGNAL);
		    if (sleep_flag > 0)
			    usleep(sleep_flag);
	    }

	    fprintf(stderr, "Waiting for server to send back data\n");
	    while ((server_read = recv(sockfd, buffer, MAXLINE, 0)) > 0)
	    {
		    memset(&cmd_ret, 0, sizeof(cmd_t));
		    memcpy(&cmd_ret, buffer, server_read);
		    sent_file_size += cmd_ret.sb.st_size;
		    fprintf(stderr, "Got info back: Server wrote %ld bytes\n", cmd_ret.file_written);
	    }

	    if (sent_file_size <= 0)
		    fprintf(stderr, "Did not get info back\n");

	    // close the file
	    close(fd);

	    close(sockfd);

	    if (is_verbose)
		    fprintf(stderr, "%ld bytes written to the socket %d\n", bytes_written, sockfd);

	    if (bytes_read < 0)
	    {
		    perror("Read from sockfd failed");
		    fprintf(stderr, "Invalid password or unable to connect to server\n");
	    }

	/* After all of this, we need to check if the file was written completely without any data loss.
	    Once we closed the sockfd, the server should stop reading from the socket. 
	    It will then send the info back to us.*/

	    sockfd = get_socket(ip_addr, ip_port); 
	    memset(buffer, 0, sizeof(buffer));
	    /*
	    while((bytes_read = read(sockfd, buffer, sizeof(buffer))) > 0)
	    {
		    memcpy(&cmd_ret, buffer, bytes_read);

		    if (cmd_ret.file_written < cmd.sb.st_size)
		    {
			    fprintf(stderr, "Bytes were lost! Resending\n");
		    }
		    else
			    fprintf(stderr, "All bytes were sent successfully!\n");
	    }
	    */
	    fprintf(stderr, "original: %ld\tserver's: %ld\n", cmd.sb.st_size, cmd_ret.file_written);

	    break;
    }

    if (cmd.sb.st_size != cmd_ret.file_written)
    {
	    pthread_create(&tid, NULL, thread_put, cmd.name);
    }

    // close the socket
    close(sockfd);

    pthread_exit((void *) EXIT_SUCCESS);
}

void
list_dir(void)
{
    cmd_t cmd;
    int sockfd;
    ssize_t bytes_read;
    ssize_t bytes_written;
    char buffer[MAXLINE];

    if (is_verbose)
	    printf("dir from server: %s \n", cmd.cmd);

    // get the new socket to the server (get_socket(...)
    sockfd = get_socket(ip_addr, ip_port);

    if (sockfd < 0)
	    fprintf(stderr, "get_socket failed\n");

    strcpy(cmd.cmd, CMD_DIR);
    strcpy(cmd.pass, hash_pass);
    strcpy(cmd.username, username);

    // write the command to the socket

    bytes_written = write(sockfd, &(cmd), sizeof(cmd_t));
    if (is_verbose)
	    printf("%ld\n", bytes_written);

    // loop reading from the socket, writing to the file
    //   until the socket read is zero
    memset(buffer, 0, sizeof(buffer));
    while((bytes_read = read(sockfd, buffer, MAXLINE)) > 0)
    {
	    write(STDOUT_FILENO, buffer, bytes_read);
    }

    if (bytes_read < 0)
    {
	    perror("Read from sockfd failed");
	    fprintf(stderr, "Invalid password or unable to connect to server\n");
    }

    if (is_verbose)
	    printf("%ld\n", bytes_read);

    // close the socket
    close(sockfd);
}

void
change_dir(char* directory)
{
	cmd_t cmd;
	int sockfd;
	
	if (is_verbose)
		fprintf(stderr, "cd from server: %s %s\n", cmd.cmd, directory);

	sockfd = get_socket(ip_addr, ip_port);

	strcpy(cmd.cmd, CMD_CD);
	strcpy(cmd.directory, directory);
	strcpy(cmd.pass, hash_pass);
	strcpy(cmd.username, username);

	write(sockfd, &(cmd), sizeof(cmd_t));

	close(sockfd);
}

void *
thread_get(void *info)
{
    char *file_name = (char *) info;

    // detach this thread 'man pthread_detach' Look at the EXMAPLES
    pthread_detach(pthread_self());

    // process one file
    get_file(file_name);

    pthread_exit(NULL);
}

void *
thread_put(void *info)
{
    char *file_name = (char *) info;

    // detach this thread 'man pthread_detach' Look at the EXMAPLES
    pthread_detach(pthread_self());

    // process one file
    put_file(file_name);

    pthread_exit(NULL);
}
