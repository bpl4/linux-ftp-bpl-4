# A Basic Secure FTP program

This program acts like an FTP (File Transfer Protocol) program. It consists of a server and a client. The client program can send and retrieve files to and from the server and can also display the active working directory where the server program is running. The server is secured with a required password that the client program must give to the server. The passwords are hashed via yescrypt algorithm and used to compare the client's and server's password for validation.

This project is a [fork of Rockem Sockets](https://gitlab.cecs.pdx.edu/bpl4/cs333-rockem_sockets-bpl4), a project for CS333: Intro to Operating Systems at Portland State University Winter Term 2024. The program was made with the lectures and help of Professor Chaney (rchaney@pdx.edu) and from the class textbook, "The Linux Programming Interface" by Michael Kerrisk. Slight changes and additions of features and basic security were made to work on any device and network when used properly. See more below.

## Features

- Send files from the client to the server using the `put` command.
- Receive files from the server to the client using the `get` command.
- Display the server's working directory using the `dir` command.
- Change the server's working directory using the `cd` command.
- Username and Password login for secure access to the server.
- See list of commands and usage with the -h option.

## Getting Started

Before using, you must port forward the desired ports for TCP on the router of the system that will be running the server program. You must also configure the firewall to allow incoming and outgoing connections and data. If using a virtual machine, you must also enable port forwarding in the virtual machine's settings.

This program has been tested on Ubuntu 22.04.4 LTS and Mint 21.3 (codename Virginia) Cinnamon Edition. If you are using Mint, you must run "sudo apt install libc6-dev" since this is not included with Mint.

It is recommended that the server computer is connected via ethernet directly to the router/modem. If connected wirelessly or connected to a wireless access point, there will be possible data loss during file transfers.

As of now, the current Raspberry Pi OS (tested on a Raspberry Pi 4) does not support crypt_rn() and is the only known incompatible system. 

To get started, you can run:
```bash
./rockem_server -h
```
and
```bash
./rockem_client -h
```

### Basic Server Setup

To run the server, execute the following command:

```bash
./rockem_server -p <port_number>
```

You will be prompted to enter a username and password for ther server. Once entered, the server will run indefinitely until the user enters `exit`. It will actively wait for connections from any client. If the user does not specify a port, the default port, 10001, will be used. After running the server, you can check to see if the server can be connected by visiting https://canyouseeme.org and enter the port number that was used to run with the server or follow the Basic Client Setup below.

### Basic Client Setup

To run the client program, use the following command:

```bash
./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c <command> ...
```
It is required provide a valid IPv4 address to the client program. The IPv4 address must be a public address of the system that is running the server. You can find the server's address by visiting https://www.whatismyip.com on the network where the server is running.

It is also required to provide a password. If the password provided is invalid (i.e. Do not match the server's password), the client program will immediately exit doing nothing or writing 0 bytes into the file that the user is getting. 

The following commands are supported for the `-c` option:

- `dir` - Displays the server's working directory.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c dir
    ```

- `put` - Sends files from the client to the server.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c put file1.txt file2.txt ...

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c put zipped_archive.tar.gz

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c put image.jpg videos.mp4 music.mp3 ...
    ```

- `get` - Receives files from the server to the client.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c get file1.txt file2.txt ...

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c get zipped_archive.zip

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c get 'Backstreet Boys - I Want It That Way.wav'
    ```
- `cd` - Change the server's current working directory.

    **Usage**:

    ```bash
    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd homework_folder

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd ..
    
    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd 

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd ~

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd ./homework_folder

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd ~/videos_of_dogs/

    ./rockem_client -i <public_IPv4_address> -p <port_number> -n <username> -l <password> -c cd ../FTP_Project
    ```
### Details

The program is designed to be a multi-threaded program. When a client sends or requests to get multiple files, the client program will create each thread to handle each file transfers. When the server receives these files, it will also create threads to handle each files in parallel, increasing speed and efficiency of writing and reading files onto the disk. The program is able to transfer compressed files (zip and tar files), music files (.mp3, .flac, .wav, etc.), images (.png, .jpg, etc.), videos (.mp4, .vlc, etc.), and regular files (.txt, .cpp, .c, etc.). 

One important feature that was lacking from the [Rockem Sockets](https://gitlab.cecs.pdx.edu/bpl4/cs333-rockem_sockets-bpl4) program was basic security features like a password to access the server. Early in testing when it was still a class project, the server was pinged numerous times by foreign clients. This fork adds the password feature to prevent foreign clients from potentially accessing and stealing files from the server. The passwords are hashed using the yescrypt algorithm and are used to compare the client's password with the server's password. 

Another feature this fork adds is the ability to change the working directory of the server. Previously, it was not possible to move the server's working directory, which can be an issue when we want to place files in a particular folder. The change directory command closely resembles the linux `cd` command. This way, we no longer need to run multiple server programs on different ports for each directory we are interested in.

### Known Issues

- There will be a chance that if you get/put a file(s), you may create a "holey" file or the file may be missing some bytes. This occurs whenever the file that we are putting or getting already exists in the target directory. We can fix this by deleting that file from the target directory before performing the put/get. This issue also occurs when the server computer is connected wirelessly (by wifi or connected to a WiFi extenstion point like a Google Nest Wifi router). It is recommended to connect the server computer directly to the router/modem to minimize the bytes lost through file transfers. 

- There is a chance that the server will write an empty file when the client performs a put operation. This issue also seems to be present when the client is running linux on a virtual machine (like VirtualBox). The fix is to enable port forwarding in the virtual machine's network settings. For VirtualBox, after selecting the desired Linux image, go to it's Settings>Network>Advanced>Port Forwarding and add a rule to allow a port from which the server is using.    

- If we put or get a file that does not exist, the server or client will create an empty file with the name anyways. This is normal and a simple fix would be to fork a process and exec() rm on that file.   

- If you get an error where CRYPT_OUTPUT_SIZE is undefined/does not exist, then you either do not have the latest glibc or you are using an incompatible OS or system (like the Raspberry Pi OS).  



